---
title: Mach-O Header
---

# Types
* `struct`[`mach_header`](#mach_header)
* `struct`[`mach_header_64`](#mach_header_64)
* `struct`[`load_command`](#load_command)
* `struct`[`lc_str`](#lc_str)

* `typdef struct`[`mach_header_t`](#mach_header_t)
* `typdef struct`[`mach_header_32_t`](#mach_header_32_t)


# Definitions

* `#define`[`MACH_TYPE_UNKNOWN`](#MACH_TYPE_UNKNOWN)

* `#define`[`MACH_TYPE_OBJECT`](#MACH_TYPE_OBJECT)
* `#define`[`MACH_TYPE_EXECUTE`](#MACH_TYPE_EXECUTE)
* `#define`[`MACH_TYPE_FVMLIB`](#MACH_TYPE_FVMLIB)
* `#define`[`MACH_TYPE_CORE`](#MACH_TYPE_CORE)
* `#define`[`MACH_TYPE_PRELOAD`](#MACH_TYPE_PRELOAD)
* `#define`[`MACH_TYPE_DYLIB`](#MACH_TYPE_DYLIB)
* `#define`[`MACH_TYPE_DYLINKER`](#MACH_TYPE_DYLINKER)
* `#define`[`MACH_TYPE_BUNDLE`](#MACH_TYPE_BUNDLE)
* `#define`[`MACH_TYPE_DYLIB_STUB`](#MACH_TYPE_DYLIB_STUB)
* `#define`[`MACH_TYPE_DSYM`](#MACH_TYPE_DSYM)
* `#define`[`MACH_TYPE_KEXT_BUNDLE`](#MACH_TYPE_KEXT_BUNDLE)
* `#define`[`MACH_TYPE_FILESET`](#MACH_TYPE_FILESET)


# Discussion


# Types

## mach_header_64
```c
struct mach_header {
    uint32_t        magic;
    cpu_type_t      cputype;
    cpu_subtype_t   cpusubtype;
    uint32_t        filetype;
    uint32_t        ncmds;
    uint32_t        sizeofcmds;
    uint32_t        flags;
    uint32_t        reserved;
};
```
Mach-O 64-bit Header struct. This appears at the very top of Mach-O files and sets out the format of the file, and how to parse stuff.

To avoid any overflows or crashes, one should check that magic before doing any operations involving the reserved property, or offsets relative to the end of the Mach-O header, as this is not present in 32-bit Mach-O’s.

**Members**

* `uint32_t` `magic`
    - Mach magic number indentifier, 0xfeedface, 0xfeedfacf.
* [`cputype`](/projects/libhelper/mach-o-parser/machine-definitions#cpu_type_t) `cputype`
    - CPU type specifier.
* [`cpu_subtype_t`](/projects/libhelper/mach-o-parser/machine-definitions#cpu_subtype_t) `cpusubtype`
    - CPU subtype specifier.
* `uint32_t` `filetype`
    - Mach-O file type, e.g. [`MACH_TYPE_EXECUTE`](#MACH_TYPE_EXECUTE)
* `uint32_t` `ncmds`
    - Number of [`load_command`](#load_command)'s trailing the Mach Header.
* `uint32_t` `sizeofcmds`
    - Total size in bytes of the load command structures trailing the Mach Header.
* `uint32_t` `reserved`
    - Reserved.

Defined In: XNU
Since: [Libhelper v3.0.0](/projects/libhelper/releases/3.0.0)

## mach_header
```c
struct mach_header {
    uint32_t        magic;
    cpu_type_t      cputype;
    cpu_subtype_t   cpusubtype;
    uint32_t        filetype;
    uint32_t        ncmds;
    uint32_t        sizeofcmds;
    uint32_t        flags;
};
```
Mach-O 32-bit Header struct. This appears at the very top of Mach-O files and sets out the format of the file, and how to parse stuff. The 32-bit header differs from the 64-bit as it does not contain the `reserved` member.

**Members**

* `uint32_t` `magic`
    - Mach magic number indentifier, 0xfeedface, 0xfeedfacf.
* [`cputype`](/projects/libhelper/mach-o-parser/machine-definitions#cpu_type_t) `cputype`
    - CPU type specifier.
* [`cpu_subtype_t`](/projects/libhelper/mach-o-parser/machine-definitions#cpu_subtype_t) `cpusubtype`
    - CPU subtype specifier.
* `uint32_t` `filetype`
    - Mach-O file type, e.g. [`MACH_TYPE_EXECUTE`](#MACH_TYPE_EXECUTE)
* `uint32_t` `ncmds`
    - Number of [`load_command`](#load_command)'s trailing the Mach Header.
* `uint32_t` `sizeofcmds`
    - Total size in bytes of the load command structures trailing the Mach Header.

Defined In: XNU
Since: [Libhelper v3.0.0](/projects/libhelper/releases/3.0.0)

## mach_header_t
```c
typedef struct mach_header_64           mach_header_t
```
Libhelper redefinition of the Mach-O 64-bit header.

Defined In: Libhelper
Since: [Libhelper v3.0.0](/projects/libhelper/releases/3.0.0)

## mach_header_32_t
```c
typedef struct mach_header              mach_header_32_t
```
Libhelper redefinition of the Mach-O 32-bit header.

Defined In: Libhelper
Since: [Libhelper v3.0.0](/projects/libhelper/releases/3.0.0)




# Definitions

## MACH_TYPE_UNKNOWN
```c
#define MACH_TYPE_UNKNOWN       0x0
```
Defines a Mach-O file of unknown type.

Since [3.0.0](/projects/libhelper/releases/3.0.0)


## MACH_TYPE_OBJECT
```c
#define MACH_TYPE_OBJECT        0x1
```
Defines an Mach-O object file.

Since [3.0.0](/projects/libhelper/releases/3.0.0)


## MACH_TYPE_EXECUTE
```c
#define MACH_TYPE_EXECUTE       0x2
```
Defines a Mach-O executable.

Since [3.0.0](/projects/libhelper/releases/3.0.0)


## MACH_TYPE_FVMLIB
```c
#define MACH_TYPE_FVMLIB        0x3
```
Defines a Mach-O fixed VM Shared Library.

Since [3.0.0](/projects/libhelper/releases/3.0.0)


## MACH_TYPE_CORE
```c
#define MACH_TYPE_CORE          0x4
```
Defines a Mach-O Core file.

Since [3.0.0](/projects/libhelper/releases/3.0.0)


## MACH_TYPE_PRELOAD
```c
#define MACH_TYPE_PRELOAD       0x5
```
Defines a Mach-O Preloaded Executable File.

Since [3.0.0](/projects/libhelper/releases/3.0.0)


## MACH_TYPE_DYLIB
```c
#define MACH_TYPE_DYLIB         0x6
```
Defines a Mach-O Dynamic Library.

Since [3.0.0](/projects/libhelper/releases/3.0.0)


## MACH_TYPE_DYLINKER
```c
#define MACH_TYPE_DYLINKER      0x7
```
Defines a Mach-O Dynamic Link Editor.

Since [3.0.0](/projects/libhelper/releases/3.0.0)


## MACH_TYPE_BUNDLE
```c
#define MACH_TYPE_BUNDLE        0x8
```
Defines a Mach-O Dynamic Bundle File.

Since [3.0.0](/projects/libhelper/releases/3.0.0)


## MACH_TYPE_DYLIB_STUB
```c
#define MACH_TYPE_DYLIB_STUB    0x9
```
Defines a Mach-O Shared Library stub for static linking.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## MACH_TYPE_DSYM
```c
#define MACH_TYPE_DSYM          0xa
```
Defines a Mach-O Debugging companion file.

Since [3.0.0](/projects/libhelper/releases/3.0.0)


## MACH_TYPE_KEXT_BUNDLE
```c
#define MACH_TYPE_KEXT_BUNDLE   0xb
```
Defines a Mach-O x86_64 Kernel Extension (KEXT).

Since [3.0.0](/projects/libhelper/releases/3.0.0)


## MACH_TYPE_FILESET
```c
#define MACH_TYPE_FILESET       0xc
```
Defines a Mach-O Fileset, a Mach-O composed of other Mach-O's.

Since [3.0.0](/projects/libhelper/releases/3.0.0)
