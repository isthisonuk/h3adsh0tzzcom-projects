---
title: Introduction to HTool
---

Libarch is a disassembly framework that is designed specifically for the ARM64/AArch64 architecture. It is a lightweight and easy-to-use library that provides the tools you need to reverse engineer binaries that have been compiled for 64-bit ARM processors.

The development of Libarch was primarily driven by the need for a reliable and accurate disassembly tool for the BSc Computer Science project HTool. HTool used Libarch to provide disassembly of Mach-O binaries and iOS/macOS firmware files, and the library was instrumental in the success of the project.

### Installation

To use Libarch in your project, you can add it as a Git Submodule and include it via CMake. This allows you to easily integrate Libarch into your build system and start using its disassembly functionality.

While there is currently no support for installing Libarch via a package manager, the Git Submodule approach provides a reliable and straightforward way to use the library in your project.

In addition to using Libarch as a library, you can also develop your own disassembly tools as part of the Libarch codebase under the tools/ directory. This can be a convenient way to build on top of the existing functionality provided by Libarch and create custom disassembly tools that meet your specific needs.

### Contributing

Libarch is an open-source project that welcomes contributions from the community. If you're interested in contributing to the project, the process is straightforward: simply fork the repository, make your changes, and create a Pull Request.

I will do my best to review and merge any contributions as quickly as possible, and appreciate any contributions that can help improve the project.

While there currently is no formal code style or guidelines document, I plan to develop one soon. In the meantime, I encourage contributors to follow best practices and common coding conventions to keep the codebase clean and maintainable.

### Documentation

Documentation will be posted here in due-course. 

### Downloads

All versions of Libarch are available here! 

**Version 1.0.0-rc2**

This release represents the second Release Candidate for the Libarch Disassembler. There are no formal release notes, this tag is to provide HTool a stable commit in order to create a release candidate for that project.

| Platform | Version | Link |
| -------- | ------- | ---- |
| macOS | `libarch-100.25.85` | [Download](https://github.com/h3adshotzz/libarch/releases/download/libarch-100.25.85/libarch-100.25.85.zip) |
