---
title: Dump Mach-O Shared Libraries
---

While Shared/Dynamically linked libraries are shown with the `LC_LOAD_DYLIB` load command, they can be viewed separately with the `-L` or `--libs` command. Some extra version information is printed with this method:

[HTool `-L` command example.](https://s3.cloud-itouk.org/static/h3adsh0tzz-com/images/htool/htool-macho-libs-1.png "HTool `-L` command example.")

#### List Symbols

HTool's symbol functionality is derived partly from the design of `nm(1)`. You can list all symbols, as defined by `LC_SYMTAB` with the `-S` option, or `--symbols`. 

[HTool `-S` command example.](https://s3.cloud-itouk.org/static/h3adsh0tzz-com/images/htool/htool-macho-symbols-1.png "HTool `-S` command example.")

There are two optional flags for the `-S` option: `-sym-sect` and `-sym-dbg`. The first displays the section the symbol is defined in, and the latter display and debug symbols. Currently, these cannot be used together, but this is planned.

[HTool `-S` command example with `-sym-sect`.](https://s3.cloud-itouk.org/static/h3adsh0tzz-com/images/htool/htool-macho-symbols-2.png "HTool `-S` command example with `-sym-sect`.")
