---
title: Build Information
---

To print the current version of HTool, use `--version`. To print the help menu, either run `htool` as so, or with `--help`. You can view the docs by running `man htool` once you've installed properly.

[HTool `--version`.](https://s3.cloud-itouk.org/static/h3adsh0tzz-com/images/htool/htool-version.png "HTool `--version`.")
