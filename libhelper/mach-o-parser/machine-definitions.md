---
title: Mach-O Machine Definitions
---

# Types

* `typedef` [`cpu_type_t`](#cpu_type_t)
* `typedef` [`cpu_subtype_t`](#cpu_subtype_t)
* `typedef` [`cpu_threadtype_t`](#cpu_threadtype_t)
* `typedef` [`vm_prot_t`](#vm_prot_t)

# Definitions

* `#define`[`OSSwapInt32(x)`](#OSSwapInt32(x))

* `#define` [`CPU_ARCH_MASK`](#CPU_ARCH_MASK)
* `#define` [`CPU_ARCH_ABI64`](#CPU_ARCH_ABI64)
* `#define` [`CPU_ARCH_ABI64_32`](#CPU_ARCH_ABI64_32)

* `#define` [`CPU_SUBTYPE_MASK`](#CPU_SUBTYPE_MASK)
* `#define` [`CPU_SUBTYPE_LIB64`](#CPU_SUBTYPE_LIB64)
* `#define` [`CPU_SUBTYPE_PTRAUTH_ABI`](#CPU_SUBTYPE_PTRAUTH_ABI)


* `#define` [`CPU_TYPE_ANY`](#CPU_TYPE_ANY)
* `#define` [`CPU_TYPE_X86`](#CPU_TYPE_X86)
* `#define` [`CPU_TYPE_X86_64`](#CPU_TYPE_X86_64)
* `#define` [`CPU_TYPE_ARM`](#CPU_TYPE_ARM)
* `#define` [`CPU_TYPE_ARM64`](#CPU_TYPE_ARM64)
* `#define` [`CPU_TYPE_ARM64_32`](#CPU_TYPE_ARM64_32)


* `#define` [`CPU_SUBTYPE_ANY`](#CPU_SUBTYPE_ANY)

* `#define` [`CPU_SUBTYPE_X86_ALL`](#CPU_SUBTYPE_X86_ALL)
* `#define` [`CPU_SUBTYPE_X86_64_ALL`](#CPU_SUBTYPE_X86_64_ALL)
* `#define` [`CPU_SUBTYPE_X86_ARCH1`](#CPU_SUBTYPE_X86_ARCH1)
* `#define` [`CPU_SUBTYPE_X86_64_H`](#CPU_SUBTYPE_X86_64_H)

* `#define` [`CPU_SUBTYPE_ARM_ALL`](#CPU_SUBTYPE_ARM_ALL)
* `#define` [`CPU_SUBTYPE_ARM_V4T`](#CPU_SUBTYPE_ARM_V4T)
* `#define` [`CPU_SUBTYPE_ARM_V6`](#CPU_SUBTYPE_ARM_V6)
* `#define` [`CPU_SUBTYPE_ARM_V5TEJ`](#CPU_SUBTYPE_ARM_V5TEJ)
* `#define` [`CPU_SUBTYPE_ARM_XSCALE`](#CPU_SUBTYPE_ARM_XSCALE)
* `#define` [`CPU_SUBTYPE_ARM_V7`](#CPU_SUBTYPE_ARM_V7)
* `#define` [`CPU_SUBTYPE_ARM_V7F`](#CPU_SUBTYPE_ARM_V7F)
* `#define` [`CPU_SUBTYPE_ARM_V7S`](#CPU_SUBTYPE_ARM_V7S)
* `#define` [`CPU_SUBTYPE_ARM_V7K`](#CPU_SUBTYPE_ARM_V7K)
* `#define` [`CPU_SUBTYPE_ARM_V8`](#CPU_SUBTYPE_ARM_V8)
* `#define` [`CPU_SUBTYPE_ARM_V6M`](#CPU_SUBTYPE_ARM_V6M)
* `#define` [`CPU_SUBTYPE_ARM_V7M`](#CPU_SUBTYPE_ARM_V7M)
* `#define` [`CPU_SUBTYPE_ARM_V7EM`](#CPU_SUBTYPE_ARM_V7EM)
* `#define` [`CPU_SUBTYPE_ARM_V8M`](#CPU_SUBTYPE_ARM_V8M)

* `#define` [`CPU_SUBTYPE_ARM64_ALL`](#CPU_SUBTYPE_ARM64_ALL)
* `#define` [`CPU_SUBTYPE_ARM64_V8`](#CPU_SUBTYPE_ARM64_V8)
* `#define` [`CPU_SUBTYPE_ARM64E`](#CPU_SUBTYPE_ARM64E)

* `#define` [`CPU_SUBTYPE_ARM64_32_ALL`](#CPU_SUBTYPE_ARM64_32_ALL)
* `#define` [`CPU_SUBTYPE_ARM64_32_V8`](#CPU_SUBTYPE_ARM64_32_V8)


---
# Discussion

---
# Types

## cpu_type_t
```
typedef int             cpu_type_t;
```

## cpu_subtype_t
```
typedef int             cpu_subtype_t;
```

## cpu_threadtype_t
```
typedef int             cpu_threadtype_t;
```

## vm_prot_t
```
typedef int             vm_prot_t;
```

# Definitions

## OSSwapInt32(x)
```c
#define __APPLE__
#   define OSSwapInt32(x)       _OSSwapInt32(x)
#else
#   define OSSwapInt32(x)       bswap_32(x)
#endif
```
`OSSwapInt32()` is a platform-agnostic definitions for doing a byte swap on a 32-bit integer. For Linux, this translates to `bswap_32` which is included from `byteswap.h`.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_ARCH_MASK
```
#define CPU_ARCH_MASK       0xff000000
```
This is the default mask for architecture bits.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_ARCH_ABI64
```
#define CPU_ARCH_ABI64      0x01000000
```
Capability bits used in the definition of `cpu_type`. These are used to calculate the value of the 64-bit CPU Type’s by performing a logical OR between the 32-bit variant, and the architecture mask.

This is the 64-bit ABI.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_ARCH_ABI64_32
```c
#define CPU_ARCH_ABI64_32    0x02000000
```
Capability bits used in the definition of cpu_type. These are used to calculate the value of the 64-bit CPU Type’s by performing a logical OR between the 32-bit variant, and the architecture mask.

This is the ABI for 64-bit hardware with 32-bit types.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_TYPE_ANY
```c
#define CPU_TYPE_ANY        ((cpu_type_t) -1)
```
CPU definition of any type.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_TYPE_X86
```c
#define CPU_TYPE_X86        ((cpu_type_t) 7)
```
CPU definition for Intel x86.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_TYPE_X86_64
```c
#define CPU_TYPE_X86_64     (CPU_TYPE_X86 | CPU_ARCH_ABI64)
```
CPU definition for Intel x86_64.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_TYPE_ARM
```c
#define CPU_TYPE_ARM        ((cpu_type_t) 12)
```
CPU definition for ARM32.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_TYPE_ARM64
```c
#define CPU_TYPE_ARM64      (CPU_TYPE_ARM | CPU_ARCH_ABI64)
```
CPU definition for ARM64.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_TYPE_ARM64_32
```c
#define CPU_TYPE_ARM64_32   (CPU_TYPE_ARM | CPU_ARCH_ABI64_32)
```
CPU definition for ARM64_32.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ANY
```c
#define CPU_SUBTYPE_ANY     ((cpu_subtype_t) -1)
```
CPU subtype definition for any subtype.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_X86_ALL
```c
#define CPU_SUBTYPE_X86_ALL ((cpu_subtype_t) 3)
```
CPU subtype definition for any Intel x86 CPU type.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_X86_64_ALL
```c
#define CPU_SUBTYPE_X86_64_ALL  ((cpu_subtype_t) 3)
```
CPU subtype definition for any Intel x86_64 CPU type.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_X86_ARCH1
```c
#define CPU_SUBTYPE_X86_ARCH1   ((cpu_subtype_t) 3)
```
CPU subtype definition for Intel x86 ARCH1.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_X86_64_H
```c
#define CPU_SUBTYPE_X86_64_H    ((cpu_subtype_t) 3)
```
CPU subtype definition for Intel x86_64 H.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_ALL
```c
#define CPU_SUBTYPE_ARM_ALL     ((cpu_subtype_t) 0)
```
CPU subtype definition for any ARM32 CPU type.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_V4T
```c
#define CPU_SUBTYPE_ARM_V4T     ((cpu_subtype_t) 5)
```
CPU subtype definition for ARM32 V4T (ARMv4T).

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_V6
```c
#define CPU_SUBTYPE_ARM_V6      ((cpu_subtype_t) 6)
```
CPU subtype definition for ARM32 V6 (ARMv6).

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_V5TEJ
```c
#define CPU_SUBTYPE_ARM_V5TEJ   ((cpu_subtype_t) 7)
```
CPU subtype definition for ARM32 V5TEJ (ARMv5 TEJ)

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_XSCALE
```c
#define CPU_SUBTYPE_ARM_XSCALE   ((cpu_subtype_t) 8)
```
CPU subtype definition for ARM32 XSCALE.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_V7
```c
#define CPU_SUBTYPE_ARM_V7   ((cpu_subtype_t) 9)
```
CPU subtype definition for ARM32 V7 (ARMv7).

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_V7F
```c
#define CPU_SUBTYPE_ARM_V7F   ((cpu_subtype_t) 10)
```
CPU subtype definition for ARM32 V7F (ARMv7F).

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_V7S
```c
#define CPU_SUBTYPE_ARM_V7S   ((cpu_subtype_t) 11)
```
CPU subtype definition for ARM32 V7S (ARMv7S).

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_V7K
```c
#define CPU_SUBTYPE_ARM_V7K   ((cpu_subtype_t) 12)
```
CPU subtype definition for ARM32 V7K (ARMv7K).

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_V8
```c
#define CPU_SUBTYPE_ARM_V8   ((cpu_subtype_t) 13)
```
CPU subtype definition for ARMv8.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_V6M
```c
#define CPU_SUBTYPE_ARM_V6M   ((cpu_subtype_t) 14)
```
CPU subtype definition for ARMv6M.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_V7M
```c
#define CPU_SUBTYPE_ARM_V7M   ((cpu_subtype_t) 15)
```
CPU subtype definition for ARMv7M.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_V7EM
```c
#define CPU_SUBTYPE_ARM_V7EM   ((cpu_subtype_t) 16)
```
CPU subtype definition for ARMv7EM.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM_V8M
```c
#define CPU_SUBTYPE_ARM_V8M   ((cpu_subtype_t) 17)
```
CPU subtype definition for ARMv8M.

Since [3.0.0](/projects/libhelper/releases/3.0.0)


## CPU_SUBTYPE_ARM64_ALL
```c
#define CPU_SUBTYPE_ARM64_ALL   ((cpu_subtype_t) )
```
CPU subtype definition for all ARM64 subtypes.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM64_V8
```c
#define CPU_SUBTYPE_ARM64_V8   ((cpu_subtype_t) )
```
CPU subtype definition for ARM64 ARMv8.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM64E
```c
#define CPU_SUBTYPE_ARM64E   ((cpu_subtype_t) )
```
CPU subtype definition for ARM64e.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM64_32_ALL
```c
#define CPU_SUBTYPE_ARM64_32_ALL   ((cpu_subtype_t) )
```
CPU subtype definition for all ARM64_32 subtypes.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM64_32_V8
```c
#define CPU_SUBTYPE_ARM64_32_V8   ((cpu_subtype_t) )
```
CPU subtype definition for ARM64_32v8.

Since [3.0.0](/projects/libhelper/releases/3.0.0)


## CPU_SUBTYPE_ARM64E_MTE_MASK
```c
#define CPU_SUBTYPE_ARM64E_MTE_MASK     0xc0000000
```
CPU Subtype feature flag for ARMs Memory Tagging Extension.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM64E_PTR_AUTH_MASK
```c
#define CPU_SUBTYPE_ARM64E_PTR_AUTH_MASK     0x0f000000
```
CPU Subtype feature flag for ARMs Pointer Authentication Extension.

Since [3.0.0](/projects/libhelper/releases/3.0.0)

## CPU_SUBTYPE_ARM64E_PTR_AUTH_VERSION
```c
#define CPU_SUBTYPE_ARM64E_PTR_AUTH_VERSION     (((x) & CPU_SUBTYPE_ARM64E_PTR_AUTH_MASK) >> 24)
```
Calculate Pointer Authentication Version.

Since [3.0.0](/projects/libhelper/releases/3.0.0)



----
# LOOK MORE INTO THESE.
## CPU_SUBTYPE_MASK
```
#define CPU_SUBTYPE_MASK    0xff000000
```
This is the default mask for the CPU Subtype bits.

Since [3.0.0]()

## CPU_SUBTYPE_LIB64
```
#define CPU_SUBTYPE_LIB64   0x80000000
```
Subtype mask for 64-bit libraries.

Since [3.0.0]()

## CPU_SUBTYPE_PTRAUTH_ABI
```
#define CPU_SUBTYPE_PTRAUTH_ABI     0x80000000
```
Subtype mask for architectures with Pointer Authentication enabled.

Since [3.0.0]()
